export const environments = {
  dev: '.env',
  staging: '.env.staging',
  production: '.env.production',
};

// NODE_ENV=staging yarn start:dev
// NODE_ENV=production yarn start:dev
