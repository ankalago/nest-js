import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from "@nestjs/common";

@Injectable()
export class ParseIntCustomPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    console.log('************* metadata: ', metadata);
    const val = parseInt(value, 10);
    if (isNaN(val)) {
      throw new BadRequestException(`${value} is not a integer`);
    }
    return val;
  }
}
