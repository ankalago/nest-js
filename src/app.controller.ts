import { Controller, Get, Param, Query } from "@nestjs/common";
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  // Params
  @Get('/test1/:text')
  getTest1(@Param() text: { text: string }): string {
    return `Hello ${text.text}`;
  }

  @Get('/test2/:text')
  getTest2(@Param('text') text: string): string {
    return `Hello ${text}`;
  }

  @Get('/test3/:text/prod/:id')
  getTest3(@Param() text: { text: string; id: string }): string {
    return `Hello ${text.text} con id: ${text.id}`;
  }

  // Query String
  @Get('/test4')
  getTest4(@Query() query: { page: string; offset: string }): string {
    return `page: ${query.page} con offset: ${query?.offset}`;
  }
}
