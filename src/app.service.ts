import { Inject, Injectable } from '@nestjs/common';
import config from './config';
import { ConfigType } from '@nestjs/config';

@Injectable()
export class AppService {
  constructor(
    @Inject('API_KEY') private apiKey: string,
    @Inject('TASKS') private tasks: any[],
    @Inject(config.KEY) private configServiceType: ConfigType<typeof config>,
  ) {}
  getHello(): any {
    const apiKey = this.configServiceType.apiKey;
    const db = this.configServiceType.database.name;
    console.log('************* apiKey: ', apiKey);
    console.log('************* db: ', db);
    return {
      message: `Hello World! ${this.apiKey}`,
      data: this.tasks,
    };
  }
}
